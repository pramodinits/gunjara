<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gunjara');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>1a:neV4$owY?glB-HLPHk1w|s6R6S;+_d}OxG]%5yWdiRrROu(e6werrM/BS~vq');
define('SECURE_AUTH_KEY',  'w/t>Ryj8J1cs[~%^*Wm*&M`0Y{--=6C,Wn%1Os(&jw?xdN;.oU#71w:lkfM,{]nJ');
define('LOGGED_IN_KEY',    'aMEp-Yfkb|ok[0c5jwXyPESseYczAJZW43b/woLEGbp/~i(8wa[O)r(ToRoaK78k');
define('NONCE_KEY',        'mR}uyO:!O{S o`p| H[C~7ddFBbsk-w<+3fy_WevkA7<mrV0xOf/BeR8WgBDhKFL');
define('AUTH_SALT',        'U .LX!= sw[0Lk14bB0=.[?xSy#ebs1x]-pC>U`zig{GgKE0T;Ufr32jPd|n>fD+');
define('SECURE_AUTH_SALT', ').BrD5XX_H`)l}tta8G9 sR-y}KDpz|}QtLJpaXN$ntX2:t7$|mhs[}eW.2$Nlny');
define('LOGGED_IN_SALT',   'SVbho9(YrMQgB.3C+.oJ>y~_!,.kzD`q*t%})o-)S_4Cr=R_!JbcN?%T^~d;NMY|');
define('NONCE_SALT',       '7K1Eg7}x!r3JO#:)-G7N{[EfyB@KnKnY2z+#>8_FcEffXxBUKQD|;(-^^P(jJL4Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
