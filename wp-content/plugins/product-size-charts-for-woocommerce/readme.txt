=== Product Size charts for Woocommerce  ===

Contributors: phoeniixx
Tags: product size charts, size, chart, charts, sizes, product chart sizes, size product, product sizes, woocommerce, product, products, e-commerce, shop, premium, category , WC size charts , wc size charts , WC product size chart , wc product size chart , tab size chart , popup size chart , tabbed popup size chart , phoenix , phoenixx , phoeniixx
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This Plugin lets you display size charts on product pages, so that your customers are facilitated in selecting the right size for the item.

== Description ==

[LIVE DEMO](http://sizechart.phoeniixxdemo.com/product/ninja-silhouette/) | [PREMIUM VERSION LINK](https://www.phoeniixx.com/product/product-size-charts-for-woocommerce/)

<blockquote>
<h4>NEW</h4>
<ul>
<li>  Our plugin is compatible with the latest version of Woocommerce</li>
</ul>
<p></p>
</blockquote>


= Overview =

Different brands have varying size labels because of which often times online shoppers end up ordering the wrong-size item. The product returning process unnecessarily causes inconvenience to the customers. As an online shop owner, you would not want to annoy your customers with this petty issue which indeed could be easily fixed by installing Product Size Charts Plugin.
The plugin allows you to place custom sizes on each of your product pages so that your customers could easily pick up the right size of the item they want to buy.

= Free Features: =

* <strong>NEW</strong> The plugin is compatible with our <strong> Support Ticket System by Phoeniixx plugin </strong>
* Option to enable or disable size chart plugin.
* Option to create unlimited number of Size Charts.
* Created Size charts will be shown in tabs on single product page.
* Each product page would require to have a unique Size Chart.

[FREE VERSION DOCUMENTATION LINK](http://hemani.codiixx.com/wp-content/uploads/2017/10/Product-Size-Chart-Free-Documentation.pdf)

<strong>Take a look at how to create size chart in Woocommerce</strong>
[youtube https://youtu.be/4YqEdZTTnrE]

= Premium Version Features: =

* Option to enable or disable size chart plugin.
* Option to create unlimited number of Size Charts.
* Option to display created size chart as **Tab**, **Popup** and  **Tabbed Popup**.
* Option to set size chart on the basis of product categories.
* Option to show the size chart in a Widget. 
* Option to set same size chart for all the products.
* Option to set more than one size chart for single product. 
* Advanced styling options.

[LIVE DEMO](http://sizechart.phoeniixxdemo.com/product/ninja-silhouette/) | [PREMIUM VERSION LINK](https://www.phoeniixx.com/product/product-size-charts-for-woocommerce/)

= Premium WordPress Themes =
Here is our premium wordpress theme

* <a href="https://www.phoeniixx.com/product/foody/">Foody Themes</a>
* <a href="https://www.phoeniixx.com/product/jstore-theme/">Jstore Themes</a>
* <a href="https://www.phoeniixx.com/product/eezy-store/">Eezy Themes</a>
* <a href="https://www.phoeniixx.com/product/craze/">Craze Themes</a>

<blockquote>
<h4>Our Website</h4>
<ul>
<li>  <a href="https://www.phoeniixx.com/">https://www.phoeniixx.com/</a></li>
</ul>
<h4>Support Email</h4>
<ul>
<li>  <a href="mailto:support@phoeniixx.com">support@phoeniixx.com</a></li>
</ul>
<h4>NOTE</h4>
<ul>
<li>  Our plugin doesn't support the multisite network.
</ul>
<p></p>
</blockquote>

== Installation ==

1. Install [WordPress](https://wordpress.org/)
2. Go to your WordPress admin panel, and then to Plugins. Click on "Add New" and then upload the zip file of the plugin using the "Upload Plugin" button you can find on top of the screen.
3. For the plugin to work as it should, WooCommerce plugin has to be installed and enabled.


Product Size charts for Woocommerce adds a new entry in admin menu called "Size Charts", from which you can manage your custom Product Size Charts.

== Screenshots ==

1. Admin view: List of all Size Charts created
2. Admin view: New Size Chart
3. Admin view: Add unlimited rows and columns to the table
4. Admin view: Assign Size Chart to a product
5. Frontend view: Size Chart

== Changelog ==

= 1.5 - 27/10/2017 =
* compatible with the latest version of woocommerce and added new feature

= 1.4 - 26/09/2017 =
* video tutorial added

= 1.3 - 28/07/2017 =
* Bug fixes

= 1.2 - 23/05/2017 =
* Bug fixes

= 1.1 - 13/05/2016 =
* Security Update.

= 1.0 =
* Initial release.
