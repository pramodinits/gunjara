<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices(); ?>

<div class="woocommerce-login-wrapper">
    <h3><?php _e( 'Lost your password?', 'woocommerce' ); ?></h3>
<form method="post" class="woocommerce-ResetPassword lost_reset_password">

	<p style="text-align: center;">
            <?php echo apply_filters( 'woocommerce_lost_password_message', __( 'We will send you a link to reset your password. <br> Enter Username or Email address.', 'woocommerce' ) ); ?>
        </p>
	<!--<p><?php echo apply_filters( 'woocommerce_lost_password_message', __( 'Please enter your username or email address. You will receive a link to create a new password via email.', 'woocommerce' ) ); ?></p>-->

<!--	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">-->
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
<!--		<label for="user_login"><?php _e( 'Username or email', 'woocommerce' ); ?></label>-->
                <input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" placeholder="Enter Username or email" />
	</p>

	<div class="clear"></div>

	<?php do_action( 'woocommerce_lostpassword_form' ); ?>

	<p class="woocommerce-form-row form-row">
		<input type="hidden" name="wc_reset_password" value="true" />
		<input type="submit" class="woocommerce-Button button" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>" />
                <label class="woocommerce-form__label inline">
                    <a class="woocommerce-LostPassword lost_password" href="<?php echo home_url(); ?>/my-account/"><?php _e('Login!', 'woocommerce'); ?></a>
                </label>
	</p>

	<?php wp_nonce_field( 'lost_password' ); ?>

</form>
</div>