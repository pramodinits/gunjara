<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer main-footer" role="contentinfo">
		<div class="col-full">
                    <div class="container">
                        <div class="footer-wrapper">
                            <div class="footer-coloum1">
                            <ul>
                                <li><a title="" href="<?php echo home_url(); ?>/about/">About Us</a></li>
                                <li><a title="" href="<?php echo home_url(); ?>/contact-us/">Contact Us</a></li>
<!--                                <li><a title="" href="<?php echo home_url(); ?>/careers/">Careers</a></li>-->
                                <li><a title="" href="<?php echo home_url(); ?>/faq/">FAQ</a></li>
                            </ul>
                        </div>
                            <div class="footer-separator"></div>
                            <div class="footer-coloum2">
                            <ul>
                                <li><a title="" href="#">Terms & Conditions</a></li>
                                <li><a title="" href="#">Shipping Policy</a></li>
<!--                                <li><a title="" href="#">ipr policy</a></li>-->
                                <li><a title="" href="#">Privacy Policy</a></li>
                            </ul>
                        </div>
                          <div class="footer-separator"></div>  
                            <div class="footer-coloum3">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/loc.png">
                            <span>Oxidanz Solutions, 4015, Sobha Daisy Apt, Ibblur <br> Bangalore 560103</span>
                            <br>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/call.png">
                            <span>+91 9742213855</span><br>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/mail.png"><span>
                                <a href="mailto:info@oxidanz.com" style="color: #fff;">info@oxidanz.com</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="footer-copyright">
                        <p>
                            © <?= date('Y'); ?> - 9Gunjara - All Rights Reserved
                        </p>
                    </div>
<!--                    <div class="footer-contact">
                        <div class="contact-header">
                            <h3>Contact Details</h3>
                        </div>
                        <p>Apartment No A3/7,<br>
                            633 W2 Flat<br>
                            Austrlia, AUS 542103<br>
                            <strong>Contact:</strong> 5177885441
                            </p> 
                    </div>
                    <div class="footer-copyright">
                        <p>
                            © Gunjara 2017 <br>
Storefront designed by Gunjara
                        </p>
                    </div>-->
			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			//do_action( 'storefront_footer' ); ?>
		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">
    jQuery( "button.navbar-toggle-responsive" ).click(function() {     
   jQuery('.category-content').toggleClass("categoryShow");
});
</script>

</body>
</html>
